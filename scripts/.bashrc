#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

eval $(keychain --eval id_ecdsa)
alias mirror="sudo reflector -c US -a 6 --sort rate --save /etc/pacman.d/mirrorlist && cat /etc/pacman.d/mirrorlist"
alias von="nohup vpn.sh -c remote-il1"
alias voff="vpn.sh -d"

 alias ILnms="ssh -A catrail@10.160.252.37"
 alias MNnms="ssh -A catrail@10.63.80.84"
 alias PAnms="ssh -A catrail@10.170.252.23"
 alias TXnms="ssh -A root@10.96.254.144"
 alias TXobj="ssh root@10.96.254.39"
 alias NNEobjB="ssh root@172.21.132.215"
 alias NNEobjP="ssh root@172.21.132.214"
 alias Impact="ssh -A root@10.96.254.44"
 alias ILprobe="ssh -A catrail@10.160.252.11"
 alias CTCprobe="ssh -A catrail@10.160.252.52"
 alias FLprobe="ssh -A catrail@10.226.1.12"
 alias CAprobe="ssh -A catrail@10.40.2.30"
 alias MNprobe1="ssh -A catrail@10.63.80.81"
 alias MNprobe2="ssh -A root@10.144.17.53"
 alias KCprobe="ssh -A catrail@10.192.249.33"
 alias NNEprobe04="ssh root@172.21.132.217"
 alias NNEprobe07="ssh root@172.21.132.220"
 alias NNEprobe08="ssh root@172.21.132.221"
 alias NYprobe="ssh -A catrail@10.164.7.4"
 alias PAprobe="ssh -A catrail@10.170.252.16"
 alias TXprobe="ssh -A catrail@10.96.254.203"
 alias WAprobe="ssh -A catrail@10.58.1.12"
 alias Repo="ssh -A catrail@10.96.254.49"
 alias Impact10="ssh root@172.21.132.223"
 alias Impact09="ssh root@172.21.132.222"
 alias CTCnms="ssh -A ctrail@opennms.ctc.biz"
 alias SIB="ssh root@172.21.132.224"
 alias ImpactNNE="ssh root@10.96.254.56"
