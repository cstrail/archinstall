A script for the base install on MBR/Legacy will be available soon.
Remember that the first part of the Arch Linux install is manual, that is you will have to partition, format and mount the disk yourself. Install the base packages and make sure to inlcude git so that you can clone the repository in chroot.
A small summary:  these are for UEFI!!

If needed, load your keymap

```timedatectl set-ntp true```

Refresh the servers with  
```reflector -c "United States" --verbose -p https -l 5 --sort age --save /etc/pacman.d/mirrorlist```  
```pacman -Syy```

Partition the disk (check name with lsblk 1st!!!)  
```gdisk /dev/sda```

create new EFI partition:  
```n``` for new  
```Enter``` for default partition number  
```Enter``` for default of 1st Sector  
```+200M``` for 200 Meg  
```ef00``` for partition type for EFI System partition  

create new EXT4 partition for rest of disk:  
```n``` for new  
```Enter``` for default partition number 2  
```Enter``` for default start sector  
```Enter``` for default last sector (for remainder of disk)  
```Enter``` for default of 8300 Linux FileSystem  

write changes to disk:  
```w``` to write changes  
```y``` to proceed  

Format the partitions  
```mkfs.fat -F32 /dev/sda1```  
```mkfs.ext4 /dev/sda2```  

Mount the partitions  
```mount /dev/sda2 /mnt```  
```mkdir -p /mnt/boot/efi```  
```mount /dev/sda1 /mnt/boot/efi```  


Install the base packages into /mnt (pacstrap /mnt base linux linux-firmware git vim intel-ucode (or amd-ucode))  
```pacstrap /mnt base linux linux-firmware git vim intel-ucode``` 

Generate the FSTAB file with  
```genfstab -U /mnt >> /mnt/etc/fstab```

Chroot in with  
```arch-chroot /mnt``` 

Create Swap File - UEFI only  
```dd if=/dev/zero of=/swapfile bs=1M count=1024 status=progress```
```chmod 600 /swapfile```  
```mkswap /swapfile```  
```swapon /swapfile```
``` vim /etc/fstab``` and add new line at end with following:  /swapfile    none    swap    defaults    0 0   


Download the git repository with  
```git clone https://gitlab.com/cstrail/archinstall.git```  

Change Mods on archinstall folder  
```chmod 777 -R /archinstall/```

CD to arch-basic folder  
```cd archinstall```  

run with passing in -xxxxx or whatever you want for hostname and Username to the install-mbr.sh script.  ex:  ./install-mbr.sh archPC chris  
```./install-uefi.sh archPC chris```  

reboot and install WM or DE of choice -if running xfce pass in username  
```./xfce.sh chris```
