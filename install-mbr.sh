#!/bin/bash

ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo $1 >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $1.localdomain arch" >> /etc/hosts
echo root:password | chpasswd

# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

pacman -S --noconfirm grub networkmanager-openconnect networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools reflector base-devel linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils cups hplip alsa-utils pulseaudio bash-completion openssh rsync reflector xfce4-terminal pavucontrol networkmanager-fortisslvpn-git

# pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=i386-pc /dev/sda # replace sdx with your disk name, not the partition
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable cups
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable fstrim.timer
systemctl enable reflector.timer
#systemctl enable bluetooth
#systemctl enable tlp # You can comment this command out if you didn't install tlp, see above
#systemctl enable libvirtd
#systemctl enable firewalld
#systemctl enable acpid

useradd -m $2
echo $2:password | chpasswd
usermod -aG wheel $2

echo "$2 ALL=(ALL) ALL" >> /etc/sudoers.d/$2


/bin/echo -e "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
