A script for the base install on MBR/Legacy will be available soon.
Remember that the first part of the Arch Linux install is manual, that is you will have to partition, format and mount the disk yourself. Install the base packages and make sure to inlcude git so that you can clone the repository in chroot.

A small summary:  these are for MBR!!!

1. If needed, load your keymap
2. Refresh the servers with 
```
reflector -c "United States" --verbose -p https -l 5 --sort age --save /etc/pacman.d/mirrorlist
pacman -Syy
```
3. Partition the disk (check name with lsblk 1st!!!)
```
cfdisk /dev/sda
```
- Make a partition of swap that is 1 to 1.5 X the Ram Size.  Ex. 8GB
- Make a Linux partition of rest of size.
- WRITE - yes

4. Format the partitions - dont need to format SWAP partition
```
mkfs.ext4 /dev/sda2
mkswap /dev/sda1
swapon /dev/sda1
```


5. Mount the partitions
```
mount /dev/sda2 /mnt
```
6. Install the base packages into /mnt (pacstrap /mnt base linux linux-firmware git vim intel-ucode (or amd-ucode))
```
pacstrap /mnt base linux linux-firmware git vim intel-ucode 
```
7. Generate the FSTAB file with 
```
genfstab -U /mnt >> /mnt/etc/FSTAB
```
8. Chroot in with
```
arch-chroot /mnt
```
9. Download the git repository with 
```
git clone https://gitlab.com/cstrail/archinstall.git
```
10.  Change Mods on archinstall folder
```
chmod 777 -R /archinstall/
```

11. CD to arch-basic folder
```
cd archinstall
```
12. run with passing in -xxxxx or whatever you want for hostname and Username to the install-mbr.sh script.  ex:  ./install-mbr.sh archPC chris
```
./install-mbr.sh archPC chris
```
13. reboot and install WM or DE of choice -if running xfce pass in username
```
./xfce.sh chris
```
