#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

git clone https://aur.archlinux.org/yay.git ~/yay
cd ~/yay
makepkg -si --noconfirm

#sudo pacman -S --noconfirm xorg lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings xfce4 xfce4-goodies firefox arc-gtk-theme arc-icon-theme vlc

sudo pacman -S --noconfirm xfce4 xfce4-goodies alsa-utils android-tools arc-gtk-theme arc-icon-theme archlinux-wallpaper autoconf automake base bind binutils bison cronie cups dialog dosfstools efibootmgr evolution evolution-ews exo expect fakeroot flex freerdp garcon gcc geany  geany-plugins ghostscript git gnome-calculator gnome-keyring groff grub gvfs gvfs-mtp inetutils intel-ucode java11-openjfx keychain libreoffice-fresh libtool lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings linux linux-firmware linux-headers linux-lts luit m4 make materia-gtk-theme mlocate mousepad mtools mtpfs mysql-workbench network-manager-applet networkmanager networkmanager-openconnect nmap notepadqq npm ntfs-3g papirus-icon-theme parole patch pavucontrol pidgin pkgconf pulseaudio pycharm-community-edition python-pip reflector remmina ristretto simplescreenrecorder sudo  texinfo thunar thunar-archive-plugin thunar-media-tags-plugin thunar-volman traceroute transmission-gtk ttf-droid tumbler unzip vim vlc which xdg-user-dirs xdg-utils xf86-video-intel xf86-video-vesa xfburn xfce4-appfinder xfce4-artwork xfce4-battery-plugin xfce4-clipman-plugin xfce4-cpufreq-plugin xfce4-cpugraph-plugin xfce4-datetime-plugin xfce4-dict xfce4-diskperf-plugin xfce4-eyes-plugin xfce4-fsguard-plugin xfce4-genmon-plugin xfce4-mailwatch-plugin xfce4-mount-plugin xfce4-mpc-plugin xfce4-netload-plugin xfce4-notes-plugin xfce4-notifyd xfce4-panel xfce4-power-manager xfce4-pulseaudio-plugin xfce4-screensaver xfce4-screenshooter xfce4-sensors-plugin xfce4-session xfce4-settings xfce4-smartbookmark-plugin xfce4-systemload-plugin xfce4-taskmanager xfce4-terminal xfce4-time-out-plugin xfce4-timer-plugin xfce4-verve-plugin xfce4-wavelan-plugin xfce4-weather-plugin  xfce4-whiskermenu-plugin xfce4-xkb-plugin xfconf xfdesktop xfwm4 xfwm4-themes xorg-bdftopcf xorg-docs xorg-font-util xorg-fonts-100dpi xorg-fonts-75dpi xorg-fonts-encodings xorg-iceauth xorg-mkfontscale xorg-server xorg-server-common xorg-server-devel xorg-server-xephyr xorg-server-xnest xorg-server-xvfb xorg-sessreg xorg-setxkbmap xorg-smproxy xorg-x11perf xorg-xauth xorg-xbacklight xorg-xcmsdb xorg-xcursorgen xorg-xdpyinfo xorg-xdriinfo xorg-xev xorg-xgamma xorg-xhost xorg-xinput xorg-xkbcomp xorg-xkbevd xorg-xkbutils xorg-xkill xorg-xlsatoms xorg-xlsclients xorg-xmodmap xorg-xpr xorg-xrandr xorg-xrdb xorg-xrefresh xorg-xsetroot xorg-xvinfo xorg-xwayland xorg-xwd xorg-xwininfo xorg-xwud 

/bin/echo -e "Starting the YAY install!!"
sleep 5

yay -S --noconfirm google-chrome jmtpfs mugshot numix-gtk-theme numix-icon-theme-git ocs-url pithos postman-bin tdrop-git teams ttf-ms-fonts visual-studio-code-bin networkmanager-fortisslvpn-git yay-cache-cleanup-hook terminator

sudo systemctl enable lightdm

unzip -o /archinstall/xfceConfigs/XFCEsettings.zip -d /home/$1/.config/
sudo chown $1:$1 -R /home/$1/.config/xfce4/

#cp /archinstall/files/vpninstall.sh /home/$1/Downloads
#chmod +x /home/$1/Downloads/vpninstall.sh
#sudo /home/$1/Downloads/vpninstall.sh

#/bin/echo/ -e "View the bashrc file and add to your own if you want the aliases"
#/bin/cat archinstall/scripts/.bashrc

/bin/echo -e "You can now reboot!"
