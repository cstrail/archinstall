#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

#sudo reflector -c "United States" -a 12 --sort rate --save /etc/pacman.d/mirrorlist

git clone https://aur.archlinux.org/yay.git ~/yay
cd ~/yay
makepkg -si --noconfirm


#git clone https://aur.archlinux.org/pikaur.git
#cd pikaur/
#makepkg -si --noconfirm

#pikaur -S --noconfirm system76-power
#sudo systemctl enable --now system76-power
#sudo system76-power graphics integrated
#pikaur -S --noconfirm gnome-shell-extension-system76-power-git
#pikaur -S --noconfirm auto-cpufreq
#sudo systemctl enable --now auto-cpufreq

sudo pacman -S --noconfirm xf86-video-intel bash-completion i3 dmenu xorg lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings ttf-dejavu ttf-liberation noto-fonts nitrogen picom lxappearance pcmanfm materia-gtk-theme papirus-icon-theme firefox archlinux-wallpaper

sudo systemctl enable lightdm
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot

#### Edit the ./config/i3/config file:
# Look for the dmenu-desktop line and change to bindsym uncomment, change to Mod+d.  comment out the other. (this will use desktop shortcuts by name)
# Look for "font pango:monospace 8"  Change it to pango:notosans mono 12 or whatever you want

# Nitrogen
# exec nitrogen --restore

#picom
# exec picom -f
#################################

#May have to do Xrandr command for screen size in lightdm.conf if not right size

#
# Change terminal font to Noto Sans Mono Regular - or whatever you want

# Nitrogen App
#  add location /usr/share/backgrounds/archlinux
# to change wallpaper

# LXAPPEARANCE - or look and feel if using other dmenu
#change to materia dark and papirus icon theme

#LIGHTDM wallpapaer in term
# sudo lightdm-gtk-greeter-settings
# materia dark
# icons papirus
# background /usr/share/backgrounds/archlinux

#i3 gaps if you want it:
#   https://github.com/Airblader/i3  copy template to config file, then mod - shift - g

